#!/usr/bin/env node
const steps = require('./src/steps');
const { repeatUntilDone } = require('./src/utils/repeat-until-done');
const { print, clear } = require('./src/printer/index');

const path = process.cwd();

const main = async () => {
  for (const step of steps) {
    await repeatUntilDone(path, step.command, step.validate);
  }

  clear();
  print(`### Congratulations!`)
};

main();
