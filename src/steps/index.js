const directoryCreate = require('./directoryCreate/directory-create');
const projectInit = require('./projectInit/project-init');
const installDependencies = require('./installDependencies/install-dependencies');
const addKakuninScript = require('./addKakuninScript/add-kakunin-script');
const initKakunin = require('./initKakunin/init-kakunin');

module.exports = [directoryCreate, projectInit, installDependencies, addKakuninScript, initKakunin];
