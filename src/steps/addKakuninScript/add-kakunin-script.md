# Add Kakunin script!

 - Open "package.json" file
 - Add a new script: "kakunin": "cross-env NODE_ENV=prod kakunin"
 - Save changes

```
{
  "name": "Example",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "kakunin": "cross-env NODE_ENV=prod kakunin"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}

```
