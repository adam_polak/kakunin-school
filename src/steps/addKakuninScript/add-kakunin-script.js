const inquirer = require('inquirer');
const { print, displayCommand } = require('../../printer');
const fs = require('fs');
const path = require('path');
const config = require('../../config');

const validate = async (originPath) => {
  const directoryPath = path.join(originPath, config.directoryName, 'package.json');
  const packageJSON = JSON.parse(fs.readFileSync(directoryPath, 'utf8'));

  if (!packageJSON.scripts.hasOwnProperty('kakunin')) {
    print(`# "kakunin" script probably was not added to package.json`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return false;
  }

  if (packageJSON.scripts.kakunin !== 'cross-env NODE_ENV=prod kakunin') {
    print(`# "kakunin" script is different than expected. It should be equal to "cross-env NODE_ENV=prod kakunin".`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return false
  }

  return true;
};

module.exports = {
  command: displayCommand(__filename, 'add-kakunin-script.md'),
  validate
};
