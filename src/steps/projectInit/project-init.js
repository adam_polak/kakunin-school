const inquirer = require('inquirer');
const { print, displayCommand } = require('../../printer');
const fs = require('fs');
const path = require('path');
const config = require('../../config');

const validate = async (originPath) => {
  const directoryPath = path.join(originPath, config.directoryName, 'package.json');

  if (!fs.existsSync(directoryPath)) {
    print(`
# The directory does not exist. Make sure that the "${directoryPath}" path exists!
    `);
    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return;
  }

  return true;
};

module.exports = {
  command: displayCommand(__filename, 'project-init.md'),
  validate
};
