const inquirer = require('inquirer');
const { print, displayCommand } = require('../../printer');
const shell = require('shelljs');
const path = require('path');
const decache = require('decache');
const config = require('../../config');

const validate = async (originPath) => {
  shell.cd(path.join(originPath, config.directoryName));
  const result = shell.exec('npm run kakunin');

  if (result.stdout.indexOf('Error: function timed out, ensure the promise resolves within') >= 0) {
    print(`# You probably select "ng1" or "ng2" application type - you should choose "otherWeb" if your application is not Angular!`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return;
  }

  if (result.stderr.indexOf('ERR') >= 0) {
    print(`# Your probably did not execute "npm run kakunin init" command.`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return;
  }

  decache(path.join(originPath, config.directoryName, 'kakunin.conf'));

  const kakuninConfig = require(path.join(originPath, config.directoryName, 'kakunin.conf'));

  if (kakuninConfig.baseUrl !== 'http://todomvc.com') {
    print(`# Your baseUrl in "kakunin.conf.js" is different than: "http://todomvc.com".`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return;
  }

  return true;
};

module.exports = {
  command: displayCommand(__filename, 'init-kakunin.md'),
  validate
};
