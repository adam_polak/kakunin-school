const inquirer = require('inquirer');
const { print, displayCommand } = require('../../printer');
const fs = require('fs');
const path = require('path');
const config = require('../../config');

const validate = async (originPath) => {
  const directoryPath = path.join(originPath, config.directoryName);
  const packageJsonPath = path.join(directoryPath, 'package.json');

  const requiredLibs = ['kakunin', 'cross-env', 'protractor', 'webdriver-manager'];
  const packageJSON = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));

  if (packageJSON.dependencies === undefined) {
    print(`# Probably you did not execute a command "npm install cross-env protractor webdriver-manager kakunin --save" in the "${directoryPath}" path.`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return false;
  }

  const allInstalled = requiredLibs.reduce((allDone, lib) => {
    if (!packageJSON.dependencies.hasOwnProperty(lib)) {
      return false;
    }

    return allDone;
  }, true);

  if (!allInstalled) {
    print(`
# Probably you did not execute a command "npm install cross-env protractor webdriver-manager kakunin --save" in the "${directoryPath}" path.
# "${requiredLibs}" could not be found in "package.json" file.
  `);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return;
  }

  if (!fs.readdirSync(directoryPath).includes('node_modules')) {
    print(`# Missing "node_modules" catalog inside the directory "${directoryPath}". Make sure that you executed a command "npm install".`);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return false;
  }

  const nodeModulesPath = path.join(directoryPath, 'node_modules');
  const installedNodeModules = fs.readdirSync(nodeModulesPath);
  const filteredNodeModules = installedNodeModules.filter(module => requiredLibs.includes(module));

  if (filteredNodeModules.length !== requiredLibs.length) {
    print(`
# Probably you did not execute a command "npm install" in the "${nodeModulesPath}" path.
# "${requiredLibs}" could not be found in node_modules directory.
    `);

    await inquirer.prompt({
      type: 'confirm',
      name: 'confirm',
      message: 'Try again...'
    });

    return false
  }

  return true;
};

module.exports = {
  command: displayCommand(__filename, 'install-dependencies.md'),
  validate
};
