const inquirer = require('inquirer');
const marked = require('marked');
const TerminalRenderer = require('marked-terminal');
const shell = require('shelljs');
const markdownToString = require('../utils/markdown-to-string');

const clear = () => shell.exec('clear');

const print = (markdown) => {
  return console.log(marked(markdown))
};

const description = (filePath, markdown) => {
  return print(markdownToString(filePath, markdown));
};

const displayCommand = (filePath, markdown) => {
  return async () => {
    clear();
    description(filePath, markdown);

    return inquirer.prompt([
      {
        type: 'confirm',
        name: 'type',
        message: 'Did you finish?',
      }
    ]);
  };
};

marked.setOptions({
  renderer: new TerminalRenderer()
});

module.exports = {
  print,
  clear,
  displayCommand
};
