const fs = require('fs');
const path = require('path');
const config = require('../config');

module.exports = (filePath, markdown) => {
  const template = fs.readFileSync(path.join(path.dirname(filePath), markdown), 'utf8');

  return template.replace('DIRECTORY-NAME-PLACEHOLDER', config.directoryName);
};

