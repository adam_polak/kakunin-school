module.exports = {
  repeatUntilDone: async (path, func, verify) => {
    let done = false;

    while(!done) {
      await func();

      done = await verify(path);
    }
  }
};
